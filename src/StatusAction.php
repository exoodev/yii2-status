<?php

namespace exoo\status;

use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;

/**
 * Undocumented class.
 */
class StatusAction extends Action
{
    /**
     * @var callable a PHP callable.
     * The signature of the callable should be:
     *
     * ```php
     * function ($id, $model) {
     *     // $id is the primary key value. If composite primary key, the key values
     *     // will be separated by comma.
     *     // $model
     * }
     * ```
     */
    public $beforeSave;
    /**
     * @var callable a PHP callable.
     * The signature of the callable should be:
     *
     * ```php
     * function ($id, $model) {
     *     // $id is the primary key value. If composite primary key, the key values
     *     // will be separated by comma.
     *     // $model
     * }
     * ```
     */
    public $afterSave;
    public $attribute = 'status';

    /**
     * Changing the status of the [[Model]] model on the opposite.
     * @param integer $id Model id.
     * @return mixed
     */
    public function run($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->request->isPost) {
            throw new BadRequestHttpException('Only POST is allowed');
        }

        if ($this->beforeSave !== null) {
            call_user_func($this->beforeSave, $id, $model);
        }

        if ($model->{$this->attribute} === $model::STATUS_ACTIVE) {
            $value = $model::STATUS_INACTIVE;
        } else {
            $value = $model::STATUS_ACTIVE;
        }

        $update = $model->updateAttributes([$this->attribute => $value]);

        if ($update && $this->afterSave !== null) {
            call_user_func($this->afterSave, $id, $model);
        }

        if (!Yii::$app->request->isAjax) {
            return Yii::$app->getResponse()->redirect(Yii::$app->getUser()->getReturnUrl());
        } else {
            return $this->controller->asJson(['result' => true]);
        }
    }
}
