<?php

namespace exoo\status;

use yii\helpers\Html;
use exoo\grid\DataColumn;

/**
 * Status Column class.
 */
class StatusColumn extends DataColumn
{
    /**
     * @var string the attribute name associated with this column. When neither [[content]] nor [[value]]
     * is specified, the value of the specified attribute will be retrieved from each data model and displayed.
     *
     * Also, if [[label]] is not specified, the label associated with the attribute will be displayed.
     */
    public $attribute = 'status';
    /**
     * @var string|array|Closure in which format should the value of each data model be displayed as (e.g. `"raw"`, `"text"`, `"html"`,
     * `['date', 'php:Y-m-d']`). Supported formats are determined by the [[GridView::formatter|formatter]] used by
     * the [[GridView]]. Default format is "text" which will format the value as an HTML-encoded plain text when
     * [[\yii\i18n\Formatter]] is used as the [[GridView::$formatter|formatter]] of the GridView.
     * @see \yii\i18n\Formatter::format()
     */
    public $format = 'raw';
    /**
     * @var array the HTML attributes for the content cell tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $contentOptions = ['class' => 'uk-text-center'];
    /**
     * @var boolean the property
     */
    public $toggle = true;
    /**
     * @var string the type (label || icon)
     */
    public $type = 'icon';
    /**
     * @var string the ID of the controller that should handle the actions specified here.
     * If not set, it will use the currently active controller. This property is mainly used by
     * [[urlCreator]] to create URLs for different actions. The value of this property will be prefixed
     * to each action name to form the route of the action.
     */
    public $controller;
    /**
     * @var string
     */
    public $prompt = '';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if ($this->filter === null) {
            $this->filter = $this->grid->filterModel->getStatusList();
        }
        $this->filterInputOptions['prompt'] = $this->prompt;
    }

    /**
     * {@inheritdoc}
     */
    public function getDataCellValue($model, $key, $index)
    {
        $class = $model->getStatusClass();
        $name = $model->getStatusName();

        if ($this->type == 'label') {
            $content = Html::tag('span', $name, [
                'class' => 'uk-label' . ($class ? ' uk-label-' . $class : '')
            ]);
        } elseif ($this->type == 'icon') {
            $content = Html::tag('span', Html::icon('fas fa-circle' . ($class ? ' uk-text-' . $class : '')), [
                'uk-tooltip' => true,
                'title' => $name,
            ]);
        }

        if ($this->toggle) {
            $options['data-method'] = 'post';
            $url = $this->controller ? $this->controller . '/' . 'status' : 'status';
            return Html::a($content, [$url, 'id' => $model->id], $options);
        }

        return $content;
    }
}
