<?php

namespace exoo\status;

use yii\helpers\ArrayHelper;

/**
 * StatusHelper class.
 */
class StatusHelper
{
    public static function list($statuses): array
    {
        return ArrayHelper::map($statuses, 'id', 'name');
    }

    public static function name($statuses, $id): string
    {
        foreach ($statuses as $status) {
            if (isset($status['id']) && $status['id'] == $id) {
                if (isset($status['name'])) {
                    return $status['name'];
                }
            }
        }
        return false;
    }

    public static function cssClass($statuses, $id): string
    {
        foreach ($statuses as $status) {
            if (isset($status['id']) && $status['id'] == $id) {
                if (isset($status['class'])) {
                    return $status['class'];
                }
            }
        }
        return false;
    }

    public static function label($statuses, $status): string
    {
        $cssClass = self::cssClass($statuses, $status);
        return Html::tag('span', self::name($statuses, $status), [
            'class' => 'uk-label' . ($cssClass ? ' uk-label-' . $cssClass : '')
        ]);
    }
}
