<?php

/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 1.0.0
 */

namespace exoo\status;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Status Trait.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
trait StatusTrait
{
    private static $_statuses;

    public function __construct()
    {
        if (self::$_statuses === null) {
            self::$_statuses = ArrayHelper::index(self::getStatuses(), 'id');
        }

        return self::$_statuses;
    }

    public function getStatusesList(): array
    {
        return $this->getStatusList();
    }

    public static function getStatusList(): array
    {
        return ArrayHelper::map(self::$_statuses, 'id', 'name');
    }

    public function getStatusName(): string
    {
        return $this->getStatusValue('name');
    }

    public function getStatusClass(): string
    {
        return $this->getStatusValue('class');
    }


    public function getStatusLabel(): string
    {
        $class = $this->getStatusValue('class');
        $name = $this->getStatusValue('name');

        return Html::tag('span', $name, [
            'class' => 'uk-label' . ($class ? ' uk-label-' . $class : '')
        ]);
    }

    public function getStatusValue($key): string
    {
        foreach (self::$_statuses as $status) {
            if (isset($status['id'], $status[$key]) && $status['id'] === $this->getStatusAttribute()) {
                return $status[$key];
            }
        }
    }

    private function getStatusAttribute()
    {
        if (isset($this->status)) {
            return $this->status;
        }
        if (isset($this->status_id)) {
            return $this->status_id;
        }
    }
}
